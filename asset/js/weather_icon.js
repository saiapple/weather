var icon = new Array();
icon['thunderstorm with light rain'] = "wi-storm-showers";
icon['thunderstorm with rain'] = "wi-storm-showers";
icon['thunderstorm with heavy rain'] = "wi-storm-showers";
icon['thunderstorm'] = "wi-thunderstorm";
icon['light thunderstorm'] = "wi-thunderstorm";
icon['heavy thunderstorm'] = "wi-thunderstorm";
icon['ragged thunderstorm'] = "wi-storm-showers";
icon['thunderstorm with light drizzle'] = "wi-storm-showers";
icon['thunderstorm with drizzle'] = "wi-storm-showers";
icon['thunderstorm with heavy drizzle'] = "wi-storm-showers";
icon['moderate rain'] = "wi-day-rain";
icon['light rain'] = "wi-sleet";
icon['drizzle'] = "wi-sleet";
icon['broken clouds'] = "wi-cloudy";
icon['sky is clear'] = "wi-day-sunny";
icon['scattered clouds'] = "wi-cloud";
icon['few clouds'] = "wi-cloud";
icon['overcast clouds'] = "wi-cloudy";
icon['heavy intensity rain'] = "wi-rain-wind";