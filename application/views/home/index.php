
    <script>
        var type;
        
        $(document).ready(function () {

            var amou = $(".weather-box").length;
            var starts = 0;
            var loc;
            var name;
            var $container = $('.maincontent');
            
            weajax("",starts);
            mason();
            
            $(".btnWeather").click(function () {
            
                var dt = new Date();
                var datetime = dt.toUTCString();
                var jse = this;
                datetime = datetime.replace('GMT','');
                datetime = datetime.substring(5);
                var jse = this;
                
               
                $($(jse).siblings(".cityh").children(".weaicon")).removeClass(type);
                loc = $($(jse).parent(".weather-box")).data("location");
                $.ajax({
                    url: "https://api.openweathermap.org/data/2.5/weather?q="+loc+",MM&units=metric&APPID=b9c0f522daea0653fbcb6ff42f653f2d",
                    type: 'post',
                    dataType: 'json',
                    beforeSend: function (data) {
                        name = $($(jse).siblings(".cityh").children(".ciname")).html();
                        $($(jse).siblings(".condi")).html("Requesting weather information for "+name+".");
                         $($(jse).siblings(".weather")).slideUp(function(){
                             $container.masonry()
                         });
                    },
                    success: function (data) {
                        type = data.weather[0].description;
                        ss = type.toLowerCase();
                        type = "wi "+icon[ss];
                        $($(jse).siblings(".weather").children(".datetime").children(".datetimere")).html(":&nbsp;" + datetime);
                        $($(jse).siblings(".cityh").children(".tempre")).html(data.main.temp + "&deg;C&nbsp;");
                        $($(jse).siblings(".weather").children(".humid").children(".humidre")).html(":&nbsp;" + data.main.humidity + "%");
                        $($(jse).siblings(".weather").children(".wind").children(".windre")).html(":&nbsp;" + data.wind.speed+" km/h");
                        $($(jse).siblings(".weather").children(".clouds").children(".cloudsre")).html(":&nbsp;" + data.clouds.all);
                        $($(jse).siblings(".weather").children(".weather").children(".wea").children(".weare")).html(":&nbsp;" + data.weather[0].description);
                        $($(jse).siblings(".condi")).html("");
                        $($(jse).siblings(".cityh").children(".weaicon")).addClass(type);
                        $($(jse).siblings(".weather")).slideDown(function(){
                            $container.masonry();
                        });
                        
                    },
                    complete: function (data) {
                    },
                    error: function (data) {
                        $($(jse).siblings(".condi")).html("Failed to request a weather information for "+name+". Please try again.");
                        $container.masonry();
                    }
                });
                
            });
            
            function weajax(type, start){
                var dt = new Date();
                var datetime = dt.toUTCString();
                var jse = this;
                datetime = datetime.replace('GMT','');
                datetime = datetime.substring(5);
                loc = $($(".weather-box")[start]).data("location");
                $.ajax({
                    url: "https://api.openweathermap.org/data/2.5/weather?q="+loc+",MM&units=metric&APPID=b9c0f522daea0653fbcb6ff42f653f2d",
                    type: 'post',
                    dataType: 'json',
                    beforeSend: function (data) {
                        name = $($($(".weather-box")[start]).children(".cityh").children(".ciname")).html();
                        $($($(".weather-box")[start]).children(".condi")).html("Requesting weather information for "+name+".");
                        $container.masonry();
                    },
                    success: function (data) {
                        type = data.weather[0].description;
                        ss = type.toLowerCase();
                        type = "wi "+icon[ss];
                        $($($(".weather-box")[start]).children(".weather").children(".datetime").children(".datetimere")).html(":&nbsp;" + datetime);
                        $($($(".weather-box")[start]).children(".cityh").children(".tempre")).html(data.main.temp + "&deg;C&nbsp;");
                        $($($(".weather-box")[start]).children(".weather").children(".humid").children(".humidre")).html(":&nbsp;" + data.main.humidity + "%");
                        $($($(".weather-box")[start]).children(".weather").children(".wind").children(".windre")).html(":&nbsp;" + data.wind.speed+" km/h");
                        $($($(".weather-box")[start]).children(".weather").children(".clouds").children(".cloudsre")).html(":&nbsp;" + data.clouds.all);
                        $($($(".weather-box")[start]).children(".weather").children(".wea").children(".weare")).html(":&nbsp;" + data.weather[0].description);
                        $($($(".weather-box")[start]).children(".condi")).html("");
                        $($($(".weather-box")[start]).children(".cityh").children(".weaicon")).addClass(type);
                        $($($(".weather-box")[start]).children(".weather")).slideDown(function(){
                            $container.masonry()
                        });
                        starts++;
                        
                        if(starts<amou){
                            weajax("",starts);
                        }
                    },
                    error: function (data) {
                        $($($(".weather-box")[start]).children(".condi")).html("Failed to request a weather information for "+name+". Please try again.");
                        starts++;
                        $container.masonry();
                        if(starts<amou){
                            weajax("",starts);
                        }
                    }
                });

            }
            
            function mason(){
                $container.masonry({
                    columnWidth: '.masonry-container', 
                    itemSelector: '.masonry-container'
                });
                console.log("refresh");
            }

        });
    </script>
<div style="margin: 0;padding: 0;width: 100%;">
    <div class="jumbotron col-sm-12 col-xs-12 col-md-12 col-lg-12" style="padding: 160px 0;background-image: url('asset/img/landscape.jpg');background-repeat: no-repeat;background-size: cover;">
		<div class="container">
      <h1 style="color: rgba(250,250,250,0.6);">Welcome to my project!</h1>
      <p style="color: rgba(250,250,250,0.6);">This is an open source project and I'll share everything as soon as I finished this. Please feel free to send me a suggestion because without your thought my project is not perfect.</p>
      <p style="color: rgba(250,250,250,0.6)">Email: saiponethaaung@gmail.com</p>
	  </div>
    </div>
<div class="container">
<div class="addthis_sharing_toolbox"></div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-551ccefa30045dab" async="async"></script>
</div>
</div>
<div class="container">


    <div class="row maincontent">

    <?php
        $coun = 0;
        $cities = json_decode($cities); 
        $cities_name = json_decode($cities_name);
        foreach ($cities as $city) {
    ?>
    
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 masonry-container">
        <div class="weather-box col-md-12" data-location="<?php echo $city;?>">
            <h2 class="cityh"><span class="ciname"><?php echo $cities_name[$coun];?></span><br/><span class="tempre"></span><span class="weaicon"></span></h2>

            <div class="weather mgpd0 col-md-12">
                <div class="datetime col-md-12 mgpd0"><div class=" col-md-4 col-sm-4 col-xs-4 mgpd0">Date/Time</div><div class="col-md-8 col-sm-8 col-xs-8 mgpd0 datetimere"></div></div>
                <div class="humid col-md-12 mgpd0"><div class=" col-md-4 col-sm-4 col-xs-4 mgpd0">Humidity</div><div class="col-md-8 col-sm-8 col-xs-8 mgpd0 humidre"></div></div>
                <div class="wind col-md-12 mgpd0"><div class=" col-md-4 col-sm-4 col-xs-4 mgpd0">Wind Speed</div><div class="col-md-8 col-sm-8 col-xs-8 mgpd0 windre"></div></div>
                <div class="clouds col-md-12 mgpd0"><div class=" col-md-4 col-sm-4 col-xs-4 mgpd0">Clouds</div><div class="col-md-8 col-sm-8 col-xs-8 mgpd0 cloudsre"></div></div>
                <div class="wea col-md-12 mgpd0"><div class=" col-md-4 col-sm-4 col-xs-4 mgpd0">Weather</div><div class="col-md-8 col-sm-8 col-xs-8 mgpd0 weare"></div></div>
                <div class="clear"></div>
            </div>
            <button class="btnWeather btn btn-primary">Refresh data</button>
            <div class="condi col-md-12"></div>
        </div>

    </div>
    <?php
            $coun++;
        }
    ?>
    
</div>
</div>