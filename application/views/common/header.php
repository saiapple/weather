<!Doctype html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="description" content="<?php 
            if(isset($description)){
                echo $description;
            }else{
                echo "Welcome to my personal website. In here you can find out more about me or you can take whatever tutorial you need.";
            }            
        ?>" />
        <meta name="keywords" content="<?php 
            if(isset($keywords)){
                echo $keywords;
            }else{
                echo "tutorial, projects, angularjs, php, mysql, javascript, jquery";
            }
        ?>" />
        <title><?php echo $title;?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo $url;?>asset/weather/css/weather-icons.min.css" media="all"/>
        <link rel="stylesheet" type="text/css" href="<?php echo $url;?>asset/bootstrap/css/bootstrap.min.css" media="all"/>
        <link rel="stylesheet" type="text/css" href="<?php echo $url;?>asset/css/custom.css?v=0.01" media="all"/>
        <link rel="stylesheet" type="text/css" href="<?php echo $url;?>asset/font-awesome/css/font-awesome.min.css" media="all"/>
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
        <!-- Import external css file based on need -->
        <?php
            /* Check css variable exist or not */
            if(isset($css)){
                /* Check variable is array or not */
                if(is_array($css)){
                    /* Import all the css files */
                    foreach ($css as $style){
        ?>
        <link rel="stylesheet" type="text/css" href="<?php echo $url.'asset/css/'.$style;?>.css" media="all"/>
        <?php          
                    }
                }else{
        ?>   
        <link rel="stylesheet" type="text/css" href="<?php echo $url.'asset/css/'.$css;?>.css" media="all"/>
        <?php   
                }
                
            }
        ?>
        <link rel="icon" type="image/png" href="<?php echo $url.'asset/img/favicon.png';?>" />
        <!-- Import external css file end here -->
        <script type="text/javascript" src="<?php echo $url;?>asset/js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="<?php echo $url;?>asset/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo $url;?>asset/js/weather_icon.js"></script>
        <script type="text/javascript" src="<?php echo $url;?>asset/js/masonry.pkgd.min.js"></script>
    </head>
    
    <body itemscope itemtype="http://schema.org/WebPage">
        <?php
//            if(isset($current)){ 
       ?>
        <script>
//            $(document).ready(function(){
//                $("#<?php //echo $current;?>").addClass("active");
//            });
        </script>
        <?php
//        style="background:rgba(245,245,245,0.7);"    }
        ?>
        <div id="header" class="navbar navbar-inverse navbar-static-top">
            
            <div class="container">
                
                <a class="navbar-brand">Weather Myanmar</a>
                
                <button class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <div class="collapse navbar-collapse navHeaderCollapse">
                    
                    <ul class="nav navbar-nav navbar-right">
                        
                        <!-- <li><a href="#">Current</a></li>
                        <li><a href="#">Forecast</a></li> -->
                        
                    </ul>
                    
                </div>
                
            </div>
            
        </div>