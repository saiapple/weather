<?php

if (!defined('BASEPATH')) exit ('No direct script access allowed');

class home extends CI_Controller {

    public function index() {
        $data['title'] = "Welcome to saiapple";
        $data['url'] = $this->config->base_url();
        $data['content'] = "Welcome to my personal page. My name is as you know Sai Pone Tha Aung and I'm a passion and ambitious web developer( Actually web programmer ).";
        $data['description'] = "Welcome to Weather Myanmar projects . My name is Sai Pone Tha Aung. I'm a passion and ambitious web developer. I hope you found something useful.";
        $data['keywords'] = "weather, Myanmar, Burma";
        $data['cities'] = array("rangoon", "mandalay", "naypyidaw", "taunggyi", "myitkyina", "bago", "wakema", "nyaungdon", "taungoo", "yamethin", "tharyarwady", "dawei", "taungdwingyi", "sittwe", "shwebo", "pyu", "pyinmana", "pyay", "pyapon", "pakokku", "kyaiklat", "kyaikto", "kyaikkami", "loikaw", "letpandan", "lashio", "myingyan", "mawlamyine", "monywa", "mogok", "minbu", "myeik", "meiktila", "maymyo", "magway", "pathein", "chaung-u");
        $data['cities'] = json_encode($data['cities']);
        $data['cities_name'] = array("Yangon", "Mandalay", "Naypyidaw", "Taunggyi", "Myitkyina", "Bago","Wekema", "Nyaungdon", "Taungoo", "Yamethin", "Tharyarwady", "Dawei", "Taungdwingyi", "Sittwe", "Shwebo", "Pyu", "Pyinmana", "Pyay", "Pyapon", "Pakokku", "Kyaiklat", "Kyaikto", "Kyaikkami", "Loikaw", "Letpadan", "Lashio", "Myingyan", "Mawlamyine", "Monywa", "Mogok", "Minbu", "Myeik", "Meiktila", "Maymyo", "Magway", "Pathein", "Chaung-U");
        $data['cities_name'] = json_encode($data['cities_name']);
        
        $this->load->view('common/header', $data);
        $this->load->view('home/index');
        $this->load->view('common/footer');
    }

    public function about() {
        $data['title'] = "Welcome to itguidemm";
        $data['url'] = $this->config->base_url();
        $data['content'] = "this is about us page";
        $this->load->view('common/header', $data);
        $this->load->view('home/index');
        $this->load->view('common/footer');
    }

}
